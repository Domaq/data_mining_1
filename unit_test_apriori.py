import unittest

from apriori import (
    find_subsets,
    apriori_generate,
    apriori,
    assoc_rules,
    print_rules,
    main
)


class AprioriTest(unittest.TestCase):

    def test_find_subsets(self):
        result = find_subsets([1, 2, 3], 2)
        expected = set(
            [(1, 2),
             (2, 3),
             (1, 3)]
        )
        self.assertEqual(set(result), expected)

    def test_apriori_generate(self):
        inputList = [
            [0, 1],
            [0, 2],
            [0, 4],
            [1, 2],
            [1, 3],
            [1, 4]
        ]
        result = apriori_generate(inputList, 2)

        excepted = set(
            [(0, 1, 2),
             (0, 1, 4),
             (0, 2, 4),
             (1, 2, 3),
             (1, 2, 4),
             (1, 3, 4)]
        )
        self.assertEqual(set(result), excepted)

    def test_apriori(self):
        transaction_list = [
            (1, 1, 0, 0, 1),
            (0, 1, 0, 1, 0),
            (0, 1, 1, 0, 0),
            (1, 1, 0, 1, 0),
            (1, 0, 1, 0, 0),
            (0, 1, 1, 0, 0),
            (1, 0, 1, 0, 0),
            (1, 1, 1, 0, 1),
            (1, 1, 1, 0, 0)
        ]

        min_sup = 2

        result = apriori(transaction_list, min_sup)

        excepted = set(
            [((0,), 6),
             ((1,), 7),
             ((2,), 6),
             ((3,), 2),
             ((4,), 2),
             ((0, 1), 4),
             ((0, 2), 4),
             ((0, 4), 2),
             ((1, 2), 4),
             ((1, 3), 2),
             ((1, 4), 2),
             ((0, 1, 2), 2),
             ((0, 1, 4), 2)]
        )
        self.assertEqual(set(result), excepted)



    def test_assoc_rules(self):
        frequent_itemset =\
            [((0,), 6),
             ((1,), 7),
             ((4,), 2),
             ((0, 1), 4),
             ((0, 4), 2),
             ((1, 4), 2),
             ((0, 1, 4), 2)]
        result = []

        inputItem = [0, 1, 4]
        inputItemSup = 2
        min_conf = 0.7
        f2 = []
        assoc_rules(result, min_conf, inputItem, f2, frequent_itemset, inputItemSup)
        excepted = set(
            [(frozenset([1, 4]), frozenset([0]), 1.0),
             (frozenset([4]), frozenset([0, 1]), 1.0),
             (frozenset([0, 4]), frozenset([1]), 1.0)]
        )
        self.assertEqual(set(result), excepted)

    def test_main(self):
        min_sup = 2
        min_conf = 0.7
        result = main('apriori.csv', min_sup, min_conf)
        excepted = set(
            [(frozenset({4}),frozenset({0}), 1.0),
             (frozenset({3}), frozenset({1}), 1.0),
             (frozenset({4}), frozenset({1}), 1.0),
             (frozenset({1, 4}), frozenset({0}), 1.0),
             (frozenset({4}), frozenset({0, 1}), 1.0),
             (frozenset({0, 4}),frozenset({1}), 1.0)]
        )
        self.assertEqual(set(result), excepted)

if __name__ == '__main__':
    unittest.main()


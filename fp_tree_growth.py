import sys
import csv
import copy

def print_tree(fp_tree):
    for i in range(len(fp_tree)):
        el = fp_tree[i]
        print(i, ')  ', "value =",el.value, "sup =", el.sup,"|", el.parent, el.brother, el.children)


def print_rules(rules_set):
    for rule in rules_set:
        print(set(rule[0]), "->", set(rule[1]), "| conf=", rule[2])

class fp_tree_node:
    def __init__(self, _value, _sup, _parent, _brother):
        self.value = _value
        self.sup = _sup
        self.parent = _parent
        self.brother = _brother
        self.children = []
    value = -1
    sup = 0
    parent = -1
    brother = -1
    children = []
    
class fp_lvl_node:
    def __init__(self, _value, _sup, _brother):
        self.value = _value
        self.sup = _sup
        self.brother = _brother
    value = -1
    sup = 0
    brother = -1

def generate_fp_tree(transaction_list, con, recon):
    root_node = fp_tree_node(-1, 0, -1, -1)
    fp_tree = [root_node]
    lvls = [fp_lvl_node(con[i][0], 0, -1) for i in range(len(con))]
    for transaction in transaction_list:
        root = fp_tree[0]
        curelem = root
        curindex = 0
        for elem in transaction:
            find = False
            for elemc in curelem.children:
                if fp_tree[elemc].value == elem:
                    find = True
                    curelem = fp_tree[elemc]
                    curelem.sup += 1
                    curindex = elemc
                    lvls[recon[curelem.value]].sup += 1
                    break
            if find is False:
                node = fp_tree_node(elem, 1, curindex, lvls[recon[elem]].brother)
                fp_tree.append(node)
                curelem.children.append(len(fp_tree) - 1)
                curelem = fp_tree[len(fp_tree) - 1]
                curindex = len(fp_tree) - 1
                lvls[recon[curelem.value]].sup += 1
                lvls[recon[curelem.value]].brother = curindex
    return fp_tree, lvls

#root  -1 indesies
#lvl   elem  sup index
#node  elem sup dad brother indexes
def generate_cond_fp(fp_tree, lvls, elem, con, recon):
    root_node = fp_tree_node(-1, 0, -1, -1)
    new_tree = [root_node]
    new_lvls = [fp_lvl_node(con[i][0], 0, -1) for i in range(recon[elem])]
    new_index = [-1] * len(fp_tree)
    our_lvl = lvls[recon[elem]]
    cur_basic = fp_tree[our_lvl.brother]

    while True:
        sup = cur_basic.sup

        if cur_basic.parent != 0:
            # print(cur_basic.parent)
            cur_node = fp_tree[cur_basic.parent]
            cur_index_fp = cur_basic.parent
            if new_index[cur_index_fp] == -1:
                node = fp_tree_node(cur_node.value, sup, -1, new_lvls[recon[cur_node.value]].brother)
                new_tree.append(node)
                new_lvls[recon[cur_node.value]].sup += sup
                new_lvls[recon[cur_node.value]].brother = len(new_tree) - 1
                cur_index_new_fp = len(new_tree) - 1
                new_index[cur_index_fp] = len(new_tree) - 1
                find = False
            else:
                cur_node_new_fp = new_tree[new_index[cur_index_fp]]
                cur_node_new_fp.sup += sup
                new_lvls[recon[cur_node_new_fp.value]].sup += sup
                find = True

            while True:
                if cur_node.parent == 0:
                    if find is False:
                        new_tree[0].children.append(cur_index_new_fp)
                        new_tree[cur_index_new_fp].parent = 0
                    break
                else:
                    cur_index_fp = cur_node.parent
                    cur_node = fp_tree[cur_index_fp]
                    if new_index[cur_index_fp] == -1:

                        node = fp_tree_node(cur_node.value, sup, -1, new_lvls[recon[cur_node.value]].brother)
                        node.children.append(cur_index_new_fp)
                        new_tree.append(node)
                        new_lvls[recon[cur_node.value]].sup += sup
                        new_lvls[recon[cur_node.value]].brother = len(new_tree) - 1
                        new_tree[cur_index_new_fp].parent = len(new_tree) - 1
                        cur_index_new_fp = len(new_tree) - 1
                        new_index[cur_index_fp] = len(new_tree) - 1
                    else:
                        cur_node_new_fp = new_tree[new_index[cur_index_fp]]
                        cur_node_new_fp.sup += sup
                        new_lvls[recon[cur_node_new_fp.value]].sup += sup
                        if find is False:
                            find = True
                            cur_node_new_fp.children.append(cur_index_new_fp)
                            new_tree[cur_index_new_fp].parent = new_index[cur_index_fp]
                        cur_index_new_fp = new_index[cur_index_fp]
        if cur_basic.brother == -1:
            break
        cur_basic = fp_tree[cur_basic.brother]
    return new_tree, new_lvls


def FP_Find(fp_tree, lvls, preff, frequent_itemset, min_sup, con, recon):
    for lvl in reversed(lvls):
        if lvl.sup >= min_sup:
            local_prev = copy.copy(preff)
            local_prev.append(lvl.value)
            frequent_itemset.append((tuple(local_prev), lvl.sup))
            local_tree, local_lvls = generate_cond_fp(fp_tree, lvls, lvl.value, con, recon)
            FP_Find(local_tree, local_lvls, local_prev, frequent_itemset, min_sup, con, recon)

def assoc_rules(rules, min_conf, f1, f2, frequent_itemset, sup):
    for i in range(len(f1)):
        if len(f2) == 0 or f1[i] > max(f2):
            new_f1 = f1[:i] + f1[i + 1:]
            new_f2 = f2 + [f1[i]]
            for elem_g in frequent_itemset:
                if set(new_f1) == set(elem_g[0]):
                    conf = sup / elem_g[1]
                    if conf >= min_conf:
                        rules.append((frozenset(new_f1), frozenset(new_f2), conf))
                        if len(new_f1) > 1:
                            assoc_rules(rules, min_conf, new_f1, new_f2, frequent_itemset, sup)
                    break


def main(file_name, min_sup, min_conf):
    if min_sup <= 0:
        print("min_sup error: min_sup > 0")
        return
    if min_conf < 0 or min_conf > 1:
        print("min_conf error: min_conf in [0, 1]")
        return
    with open(file_name, 'r') as file_reader:
        bin_transaction_list = [list(map(int, row[1:])) for row in csv.reader(file_reader, delimiter=',')]
    file_reader.close()
    elements_number = len(bin_transaction_list[0])
    for transaction in bin_transaction_list:
        if len(transaction) != elements_number:
            print("error: transactions have not the same length")
            return
        if set(transaction).issubset({0, 1}) is False:
            print("error: allowed only 0 or 1 values")
            return

    con = [[i, 0] for i in range(elements_number)]
    for row in bin_transaction_list:
        for j in range(len(row)):
            con[j][1] += (1 if row[j] > 0 else 0)
    con.sort(key=lambda element: element[1], reverse=True)
    recon = [0] * elements_number
    for i in range(elements_number):
        recon[con[i][0]] = i
    tr_list = []
    for transaction in bin_transaction_list:
        temp_transaction = copy.copy(transaction)
        for i in range(len(temp_transaction)):
            transaction[recon[i]] = temp_transaction[i]
        p_row = [con[i][0] for i in range(len(transaction)) if transaction[i] > 0]
        tr_list.append(tuple(p_row))

    fp_tree, lvls = generate_fp_tree(tr_list, con, recon)

    preff = []
    frequent_itemset = []
    rules = []
    FP_Find(fp_tree, lvls, preff, frequent_itemset, min_sup, con, recon)
    for elem in frequent_itemset:
        if len(elem) > 1:
            f2 = []
            assoc_rules(rules, min_conf, list(elem[0]), f2, frequent_itemset, elem[1])
    print_rules(rules)
    return rules


if __name__ == "__main__":
    if len(sys.argv) == 4:
        main(sys.argv[1], float(sys.argv[2]), float(sys.argv[3]))
    else:
        print("argument error: use apriori.py file_name min_sup min_conf")
        sys.exit(1)


import unittest

from fp_tree_growth import (
    print_tree,
    print_rules,
    generate_fp_tree,
    generate_cond_fp,
    FP_Find,
    assoc_rules,
    main
)


def compareFPTree(fp_tree, exepted):
    if len(fp_tree) != len(exepted):
        return False
    for i in range(len(fp_tree)):
        if fp_tree[i].value != exepted[i][0] or fp_tree[i].sup != exepted[i][1] or fp_tree[i].children != exepted[i][2]:
            return False
    return True

def compareLvl(lvl, exepted):
    if len(lvl) != len(exepted):
        return False
    for i in range(len(lvl)):
        if lvl[i].value != exepted[i][0] or lvl[i].sup != exepted[i][1]:
            return False
    return True

class FPTest(unittest.TestCase):
    def test_generate_fp_tree(self):
        inputTransactions = [
            (1, 2, 0),
            (1, 0),
            (1, 2),
            (1, 2),
            (0, 3)
        ]
        con = [
            [1, 4], [2, 3], [0, 3], [3, 1]
        ]
        recon = [
            2, 0, 1, 3
        ]

        # value, sup, children
        #                -1
        #       1, 4              0, 1
        #  2, 3      0, 1             3, 1
        # 0, 1
        excepted_tree = [
            [-1, 0, [1, 5]],
            [1, 4, [2, 4]],
            [2, 3, [3]],
            [0, 1, []],
            [0, 1, []],
            [0, 1, [6]],
            [3, 1, []]
        ]
        #value, sup
        excepted_lvl = [
            [1, 4],
            [2, 3],
            [0, 3],
            [3, 1]
        ]

        result, lvl = generate_fp_tree(inputTransactions, con, recon)
        self.assertTrue(compareFPTree(result, excepted_tree))
        self.assertTrue(compareLvl(lvl, excepted_lvl))

    def test_generate_cond_fp(self):
        inputTransactions = [
            (1, 2, 0),
            (1, 0),
            (1, 2),
            (1, 2),
            (0, 3)
        ]
        con = [
            [1, 4], [2, 3], [0, 3], [3, 1]
        ]
        recon = [
            2, 0, 1, 3
        ]
        fp_tree, lvl = generate_fp_tree(inputTransactions, con, recon)
        elem = 0
        #                -1
        #       1, 2              0, 1 (x)
        #  2, 1      0, 1 (x)
        # 0, 1 (x)
        excepted_tree = [
            [-1, 0, [1]],
            [1, 2, [2]],
            [2, 1, []]
        ]
        excepted_lvl = [
            [1, 2],
            [2, 1]
        ]

        local_tree, local_lvls = generate_cond_fp(fp_tree, lvl, elem, con, recon)
        self.assertTrue(compareFPTree(local_tree, excepted_tree))
        self.assertTrue(compareLvl(local_lvls, excepted_lvl))

    def test_fp_find(self):
        inputTransactions = [
            (1, 2, 0),
            (1, 0),
            (1, 2),
            (1, 2),
            (0, 3)
        ]
        con = [
            [1, 4], [2, 3], [0, 3], [3, 1]
        ]
        recon = [
            2, 0, 1, 3
        ]
        fp_tree, lvl = generate_fp_tree(inputTransactions, con, recon)
        preff = []
        frequent_itemset = []
        min_sup = 3
        FP_Find(fp_tree, lvl, preff, frequent_itemset, min_sup, con, recon)
        excepted = set(
            [((0,), 3),
             ((1,), 4),
             ((2,), 3),
             ((2, 1), 3)]
        )

        self.assertEqual(set(frequent_itemset), excepted)

    def test_assoc_rules(self):
        frequent_itemset =\
            [((0,), 6),
             ((1,), 7),
             ((4,), 2),
             ((0, 1), 4),
             ((0, 4), 2),
             ((1, 4), 2),
             ((0, 1, 4), 2)]
        result = []

        inputItem = [0, 1, 4]
        inputItemSup = 2
        min_conf = 0.7
        f2 = []
        assoc_rules(result, min_conf, inputItem, f2, frequent_itemset, inputItemSup)
        excepted = set(
            [(frozenset([1, 4]), frozenset([0]), 1.0),
             (frozenset([4]), frozenset([0, 1]), 1.0),
             (frozenset([0, 4]), frozenset([1]), 1.0)]
        )
        self.assertEqual(set(result), excepted)

    def test_main(self):
        min_sup = 2
        min_conf = 0.7
        result = main('apriori.csv', min_sup, min_conf)
        excepted = set(
            [(frozenset({4}),frozenset({0}), 1.0),
             (frozenset({3}), frozenset({1}), 1.0),
             (frozenset({4}), frozenset({1}), 1.0),
             (frozenset({1, 4}), frozenset({0}), 1.0),
             (frozenset({4}), frozenset({0, 1}), 1.0),
             (frozenset({0, 4}),frozenset({1}), 1.0)]
        )
        self.assertEqual(set(result), excepted)

if __name__ == '__main__':
    unittest.main()


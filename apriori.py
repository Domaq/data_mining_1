import sys
import csv
import itertools


def find_subsets(s, m):
    return tuple(itertools.combinations(s, m))


def apriori_generate(f_set, i):
    c_set = []
    for j in range(len(f_set) - 1):
        for k in range(j + 1, len(f_set)):
            find = True
            for l in range(i - 1):
                if f_set[j][l] != f_set[k][l]:
                    find = False
                    break
            if find is False:
                continue
            if f_set[j][i - 1] < f_set[k][i - 1]:
                temp = f_set[j] + [f_set[k][i - 1]]
                s_set = find_subsets(temp, i - 1)
                for element_s in s_set:
                    flag = False
                    for element_f in f_set:
                        if set(element_s).issubset(set(element_f)):
                            c_set.append(tuple(temp))
                            flag = True
                            break
                    if flag is True:
                        break
    return c_set


def apriori(transaction_list, min_sup):
    frequent_itemset = []
    c_set = [(i,) for i in range(len(transaction_list[0]))]
    s_index = 1
    while len(c_set) != 0:
        f_set = []
        for i in range(len(c_set)):
            cur_sup = 0
            for transaction in transaction_list:
                find = True
                for element_c in c_set[i]:
                    if transaction[element_c] == 0:
                        find = False
                        break
                if find is True:
                    cur_sup += 1

            if cur_sup >= min_sup:
                f_set.append((c_set[i], cur_sup))
        if len(f_set) == 0:
            break
        rules_f = [list(val[0]) for val in f_set]
        c_set = apriori_generate(rules_f, s_index)
        s_index += 1

        frequent_itemset = frequent_itemset + f_set
    return frequent_itemset


def print_rules(rules_set):
    for rule in rules_set:
        print(set(rule[0]), "->", set(rule[1]), "| conf=", rule[2])


def assoc_rules(rules, min_conf, f1, f2, frequent_itemset, sup):
    for i in range(len(f1)):
        if len(f2) == 0 or f1[i] > max(f2):
            new_f1 = f1[:i] + f1[i + 1:]
            new_f2 = f2 + [f1[i]]
            for elem_g in frequent_itemset:
                if set(new_f1) == set(elem_g[0]):
                    conf = sup / elem_g[1]
                    if conf >= min_conf:
                        rules.append((frozenset(new_f1), frozenset(new_f2), conf))
                        if len(new_f1) > 1:
                            assoc_rules(rules, min_conf, new_f1, new_f2, frequent_itemset, sup)
                    break



def main(file_name, min_sup, min_conf):
    if min_sup <= 0:
        print("min_sup error: min_sup > 0")
        return
    if min_conf < 0 or min_conf > 1:
        print("min_conf error: min_conf in [0, 1]")
        return
    with open(file_name, 'r') as file_reader:
        transaction_list = [tuple(map(int, row[1:])) for row in csv.reader(file_reader, delimiter=',')]
    file_reader.close()

    element_n = len(transaction_list[0])
    for transaction in transaction_list:
        if len(transaction) != element_n:
            print("error: transactions have not the same length")
            return
        if set(transaction).issubset({0, 1}) is False:
            print("error: allowed only 0 or 1 values")
            return

    frequent_itemset = apriori(transaction_list, min_sup)
    rules = []
    for elem in frequent_itemset:
        f2 = []
        assoc_rules(rules, min_conf, list(elem[0]), f2, frequent_itemset, elem[1])
    print_rules(rules)
    return rules


if __name__ == "__main__":
    if len(sys.argv) == 4:
        main(sys.argv[1], float(sys.argv[2]), float(sys.argv[3]))
    else:
        print("argument error: use apriori.py file_name min_sup min_conf")
        sys.exit(1)